package com.polygmirror.simple.service;

import com.polygmirror.simple.model.Question;
import com.polygmirror.simple.model.ResumeSetting;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Description
 * date: 2022/8/3
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public abstract class AbstractQuestionMatchService {

    /**
     * 抽象问题匹配方法
     * @param resumeSetting
     * @param allQuestionList
     * @return
     */
    public abstract List<Question> questionMatch(ResumeSetting resumeSetting, List<Question> allQuestionList);

    /**
     * 先按照输入用户的技术栈进行问题匹配，然后根据随机匹配和连环炮匹配模式进行面试题的抽取
     * @param tagList
     * @param allQuestionList
     * @return
     */
     synchronized List<Question> matches(List<String> tagList, List<Question> allQuestionList){
        Set<String> tagSet = new HashSet<>();
        tagList.stream().forEach(tag-> tagSet.add(tag.toLowerCase()));

        List<Question> questionList = new ArrayList<>();
        for (Question question : allQuestionList){
            List<String> tags = question.getQuestionTags();
            tags.addAll(question.getCategoryTags());
            for (String str : tags){
                if(tagSet.contains(str.toLowerCase())){
                    questionList.add(question);
                    break;
                }
            }
        }

        return questionList;
    }
}
