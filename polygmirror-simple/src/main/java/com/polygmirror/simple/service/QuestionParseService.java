package com.polygmirror.simple.service;

import com.polygmirror.simple.model.Question;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Description
 * date: 2022/8/3
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class QuestionParseService {

    /**
     * 根据问题文件路径扫描问题内容构建问题列表模型
     * @param questionPath
     * @throws IOException
     */
    public List<Question>  scanQuestion(String questionPath) throws IOException {
        File file = new File(questionPath);
        if(!file.exists()){
            //todo throw exception
        }
        List<Question> questionList = new ArrayList<>();

        File [] categoryFiles = file.listFiles();
        for (File categoryFile : categoryFiles){
            scanFile(categoryFile,questionList);
        }

        return questionList;
    }


    private void scanFile(File file,List<Question> questionList) throws IOException {
        if(file.isDirectory()){

            File [] childFiles = file.listFiles();
            for (File childFile : childFiles){
                scanFile(childFile,questionList);
            }
        }else {
            List<String> tagList = new ArrayList<>();
            File tmp = file;
            while (!tmp.getParentFile().getName().equals("question")){
                tagList.add(tmp.getParentFile().getName());
                tmp = tmp.getParentFile();
            }


            List<String> questionLineList = FileUtils.readLines(file,"utf-8");
            if(questionLineList != null && !questionLineList.isEmpty()){
                int index = 1;
                for (String questionLine : questionLineList){
                    if(!questionLine.contains("[")){
                        continue;
                    }

                    List<Question> questions = buildQuestionChainList(questionLine);
                    int finalIndex = index;
                    questions.forEach(question -> {
                        if(file.getName().endsWith(".txt")){
                            question.setQuestionType(file.getName().replace(".txt",""));
                        }else {
                            question.setQuestionType(file.getName());
                        }
                        question.setQuestionIndex(finalIndex);
                        question.setCategoryTags(tagList);
                    });
                    index++;
                    questionList.addAll(questions);
                }
            }
        }
    }


    /**
     * 根据单行面试连环炮构建面试列表
     * @param questionLine
     * @return
     */
    private List<Question> buildQuestionChainList(String questionLine){
        List<Question> questionList = new ArrayList<>();
        String [] array = questionLine.split("],");

        List<String> tagsList = buildTagsList(array[array.length - 1]);
        for (int i = 0;i < array.length - 1;i++){

            Question question = new Question();
            question.setQuestionTags(tagsList);
            question.setOrder(i+1);
            if(array[i].startsWith("[")){
                question.setQuestionContent(array[i].replace("[",""));
            }else {
                question.setQuestionContent(array[i]);
            }

            questionList.add(question);
        }
        return questionList;

    }

    private List<String> buildTagsList(String str){
        str = str.replace("[","");
        str = str.replace("]","");
        String [] array = str.split(",");
        List<String> tagList = new ArrayList<>();
        for (String strTag : array){
            tagList.add(strTag);
        }
        return tagList;
    }


}
