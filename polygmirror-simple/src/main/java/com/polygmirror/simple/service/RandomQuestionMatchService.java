package com.polygmirror.simple.service;

import com.polygmirror.simple.model.Question;
import com.polygmirror.simple.model.ResumeSetting;

import java.security.SecureRandom;
import java.util.*;

/**
 * Description
 * 面试题随机抽取算法实现
 * date: 2022/8/3
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class RandomQuestionMatchService extends AbstractQuestionMatchService{

    @Override
    public List<Question> questionMatch(ResumeSetting resumeSetting, List<Question> allQuestionList) {
        List<Question> preMatchQuestionList = matches(resumeSetting.getTags(),allQuestionList);
        Set<Integer> questionIndexSet = new HashSet<>();
        Random random = new SecureRandom();
        int count = 0;
        while (count < resumeSetting.getExpectCount()){
            Integer index = random.nextInt(preMatchQuestionList.size());
            if(!questionIndexSet.contains(index)){
                questionIndexSet.add(index);
                count++;
            }
        }

        List<Question> resultList = new ArrayList<>();
        for (int i = 0;i < preMatchQuestionList.size();i ++){
            if(questionIndexSet.contains(i)){
                resultList.add(preMatchQuestionList.get(i));
            }
        }
        return resultList;
    }
}
