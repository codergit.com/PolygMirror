package com.polygmirror.simple.model;

import java.util.List;

/**
 * Description
 * date: 2022/8/3
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class Question {
    /**
     * 一级目录名+二级目录名共同构成
     * 目录类型级别的问题标签
     */
    private List<String> categoryTags;



    /**
     * 每个连环炮问题的标签，连环炮问题公用
     */
    private List<String> questionTags;

    /**
     * 文件名作为问题类型
     */
    private String questionType;

    /**
     *
     * 连环炮问题在文件中的索引顺序
     */
    private Integer questionIndex;

    /**
     * 标示连环炮问题的相对顺序
     */
    private Integer order;

    /**
     * 问题内容
     */
    private String questionContent;

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public List<String> getCategoryTags() {
        return categoryTags;
    }

    public void setCategoryTags(List<String> categoryTags) {
        this.categoryTags = categoryTags;
    }

    public List<String> getQuestionTags() {
        return questionTags;
    }

    public void setQuestionTags(List<String> questionTags) {
        this.questionTags = questionTags;
    }

    public Integer getQuestionIndex() {
        return questionIndex;
    }

    public void setQuestionIndex(Integer questionIndex) {
        this.questionIndex = questionIndex;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getCategoryKey(){
        StringBuilder builder = new StringBuilder();

        if(this.categoryTags != null && this.categoryTags.size() > 0){
            for (String categoryTag : categoryTags){
                builder.append(categoryTag);
            }
        }else {
            return "CUSTOM_QUESTION";
        }

        return builder.toString();
    }
}
