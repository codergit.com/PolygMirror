package com.polygmirror.simple.model;

import java.util.List;

/**
 * Description
 * date: 2022/8/3
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class ResumeSetting {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 项目名称
     */
    private List<String> projectNameList;
    /**
     * 技术栈
     * 对应的问题文件名或者问题标签列表
     */
    private List<String> tags;


    /**
     * 期望的面试问题数量
     * 建议:30-50之间
     */
    private Integer expectCount;



    public Integer getExpectCount() {
        return expectCount;
    }

    public void setExpectCount(Integer expectCount) {
        this.expectCount = expectCount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getProjectNameList() {
        return projectNameList;
    }

    public void setProjectNameList(List<String> projectNameList) {
        this.projectNameList = projectNameList;
    }
}
