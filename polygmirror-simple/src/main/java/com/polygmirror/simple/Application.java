package com.polygmirror.simple;

import com.polygmirror.simple.model.Question;
import com.polygmirror.simple.model.ResumeSetting;
import com.polygmirror.simple.service.AbstractQuestionMatchService;
import com.polygmirror.simple.service.QuestionParseService;
import com.polygmirror.simple.service.ChainQuestionMatchService;
import com.polygmirror.simple.service.RandomQuestionMatchService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Description
 * date: 2022/8/3
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class Application {
    public static void main(String[] args) throws IOException {

        String path = Application.class.getClassLoader().getResource("").getPath();
        path = path.split("PolygMirror")[0]+"PolygMirror/doc/question";

        QuestionParseService questionParseService = new QuestionParseService();
        List<Question> questionList =  questionParseService.scanQuestion(path);
        //随机模式
        AbstractQuestionMatchService randomMatchService = new RandomQuestionMatchService();
        //连环炮模式
        AbstractQuestionMatchService chainMatchService = new ChainQuestionMatchService();


        ResumeSetting resumeSetting = new ResumeSetting();
        resumeSetting.setUserName("shenshuai");
        List<String> abilityTagList = new ArrayList<>();
        //abilityTagList.add("开场白");
        //简历中的标签
        abilityTagList.add("Spring");
        abilityTagList.add("Java");
        abilityTagList.add("多线程");
        abilityTagList.add("JVM");
        abilityTagList.add("SpringBoot");
        abilityTagList.add("Mybatis");
        abilityTagList.add("源码");
        abilityTagList.add("Lock");
        abilityTagList.add("Mysql");
        abilityTagList.add("Redis");
        abilityTagList.add("Git");
        abilityTagList.add("分布式");
        abilityTagList.add("Net");
        resumeSetting.setTags(abilityTagList);

        resumeSetting.setExpectCount(40);

        List<Question> questions = chainMatchService.questionMatch(resumeSetting, questionList);
        System.out.println("Hello,"+resumeSetting.getUserName()+",your question has generated success.Good luck!");
        for (Question question : questions){
            System.out.println(question.getQuestionContent());
        }
    }
}
