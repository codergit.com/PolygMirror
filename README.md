# checkMirror

#### 介绍
面试随机出题，以题为镜，全面塑造知识体系。本仓库是自2020年8面试以来的全部Java后端面试题的
集合。也整合了我总结的面试连环炮系列，同时因为工作变动今年找工作也面了很多家公司，另外也收集了各个大佬的
面试技术手册，对面试官提到的所有Java后端+分布式+微服务等相关面试问题做了汇总。

目前只有问题模块，答案模块在设计中，在设计过程中也会尽可能考虑到共同维护此项目的便利性，这样的话从面试
问题到答案就形成了一定的闭环。目前会充分利用互联网资源来丰富本仓库的答案内容，尽量不让人工进行答案编辑。

#### 工程模块
1. polygmirror-core核心工程模块(暂无代码,)
2. polygmirror-simple(简单模块,当前实现逻辑主要在这里，后面会迁移到core模块)
3. polygmirror-web(暂无代码,web端工程管理面试题)

#### 安装教程

1. git clone本项目 
2. 启动polygmirror-simple模块Application的main方法

#### 使用说明

1. 面试题出题有两种模式,介绍如下:
2. 随机出题模式(查缺补漏)
3. 连环炮模式(模拟面试)
4. 参考 doc/Readme文档

#### 问题收录类目

1. 系统设计(ArchDesign,DistributionDesign,MiddleDesign,SystemDesign)
2. 数据库(MongoDB,Mysql,Oracle,Redis)
3. 设计模式(DDD,DesignPattern)
4. 中间件(Dubbo,ElasticSearch,Kafka,Middleware,Mybatis,Nacos,Netty,RocketMq,Spring,SpringBoot,SpringCloud,Tomcat,Zookeeper)
5. 网络(CND,DNS,SEC,TCPIPHTTP)
6. SDK开发语言(C,C++,GO,Java)
7. Shell(Bash,Python)
8. 闲聊问题(ShowMe)
9. Tools(Git,Maven)
10. 持续收录中(扩展到前端,大数据,算法)


#### 面试经典问题链接

1.  [https://gitee.com/explore](https://gitee.com/explore)
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 鸣谢
1. 如有大佬愿意共享面试手册资源或者面试问题可以在此登记，这里特此鸣谢。
2. 对已经收录的面试问题如有雷同还请联系我进行登记，本仓库为公益性质，不包含任何商业行为。
