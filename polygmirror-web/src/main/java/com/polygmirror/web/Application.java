package com.polygmirror.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description
 * date: 2022/8/7
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
