package com.polygmirror.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description
 * date: 2022/8/7
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@RestController
public class HelloController {
    @RequestMapping(value = "/hello")
    public String sayHell(){
        return "hello.";
    }
}
